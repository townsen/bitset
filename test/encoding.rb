#!/usr/bin/env ruby 
# encoding: utf-8
# encoding: us-ascii
# encoding: iso-8859-1
#
# Test of encoding issues
# Shows how bitset is stored from left to right LSB to MSB so byte values are
#
require 'bitset'

a = BitSet.from_bin("0000000100000010000000110000010000000101000001100000011100001000")
# a is \x80\x40\xC0\x20\xA0\x60\xD0\x10
b = BitSet.from_bin("1000000001000000110000000010000010100000011000001110000000010000")
# b is \x01\x02\x03\x04\x05\x06\x07\x08
c = BitSet.new("µΩ")
# c is \xC1\xB5\xE2\x84\xA6

def dump name, bs
  puts "#{name}: The first bit set is: #{bs.min}, the last is: #{bs.max}"
  res = bs.to_bytes
  puts ("I got size #{res.size} in encoding: #{res.encoding.name}: #{res.unpack('C*').join(',')}")
end

dump "A", a
dump "B", b
dump "C", c
