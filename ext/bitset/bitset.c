/* Taken from an original by 'MoonWolf' on RAA
 * Additional functions and Gem wrapper by Nick Townsend 2014
 */
#include "ruby.h"
#include "string.h"

static VALUE cBitSet;
static ID id_new, id_first, id_end;

struct BitSet {
  int64_t   bit_len;  /* in bits */
  int64_t   qws_len;  /* number of uint64 elements */
  int       last_bit; /* number of last bit in final element */
  uint64_t *ptr;
};

void bs_resize(struct BitSet *bits, int64_t bit_len) {

  int64_t qws_len   = (bit_len + 63) / 64;
  int     last_bit  = bit_len % 64;

  if (bits->bit_len == bit_len) {
    return;
  }
  else if (bits->bit_len > bit_len) {
    bits->ptr = REALLOC_N(bits->ptr, uint64_t, qws_len);
    if (last_bit > 0) {
      bits->ptr[qws_len-1] &= (1<<last_bit)-1;
    }
  }
  else if (bits->bit_len < bit_len) {
    bits->ptr = REALLOC_N(bits->ptr, uint64_t, qws_len);
    memset(bits->ptr+bits->qws_len, 0, qws_len - bits->qws_len);
  }
  bits->bit_len = bit_len;
  bits->qws_len = qws_len;
  bits->last_bit = last_bit;
}

int64_t bs_bytesize(struct BitSet *bits) {
  int64_t byte_len = bits->qws_len*8;
  if (bits->last_bit > 0) {
    byte_len -= 8;
    byte_len += (bits->last_bit+7)/8;
  }
  return byte_len;
}

int bs_get(struct BitSet *bits, int64_t index) {
  int64_t byte_offset;
  int64_t bit_offset;
  uint64_t c;
  int bit;

  if (index >= bits->bit_len) {
    bit = 0;
  } else {
    asm ( "mov %2, %%r8 \n"
          "shr $6, %%r8 \n"
          "mov %2, %%r9 \n"
          "and $63, %%r9 \n"
          "mov $0, %0 \n"
          "bt %%r9, (%1, %%r8, 8) \n"
          "jnc get_exit \n"
          "mov $1, %0 \n"
          "get_exit: \n"
        : "=r"(bit) : "r"(bits->ptr), "r"(index) : "cc"
    );
  }
  return bit;
}

void bs_set(struct BitSet *bits, int64_t index, int bit) {
  int64_t byte_offset;
  int64_t bit_offset;
  uint64_t c;

  if (index >= bits->bit_len) {
    bs_resize(bits, index+1);
  }

  asm volatile (
      "mov %1, %%r8 \n"
      "shr $6, %%r8 \n"
      "mov %1, %%r9 \n"
      "and $63, %%r9 \n"
      "cmp $0, %2 \n"
      "je bs_set_zero \n"
      "bts %%r9, (%0,%%r8,8) \n"
      "jmp bs_set_exit \n"
      "bs_set_zero: \n"
      "btr %%r9, (%0,%%r8,8) \n"
      "bs_set_exit: \n"
      : : "r"(bits->ptr), "r"(index), "r"(bit) : "%r8", "%r9", "cc", "memory"
  );

}

void bs_sets(struct BitSet *bits, int64_t first, int64_t last, int bit) {
  int64_t first_offset;
  int64_t last_offset;
  int64_t first_bit;
  int64_t last_bit;
  int64_t qws;
  int64_t i;
  uint64_t c;

  if (bits->bit_len < last) {
    bs_resize(bits, last);
  }

  first_offset = first / 64;
  first_bit    = first % 64;
  last_offset  = last  / 64;
  last_bit     = last  % 64;
  qws = last_offset - first_offset + 1;

  if (first_bit==0 && last_bit==63) {
    memset(&bits->ptr[first_offset], bit ? 0xffffffffffffffff : 0x0, qws);
    return;
  }

  if (last-first < 63) {
    for(i=first; i<=last; i++) {
      bs_set(bits, i, bit);
    }
    return;
  }

  if (first_bit != 0) {
    c = bits->ptr[first_offset];
    if (bit) {
      c |= ~((1 << first_bit) - 1);
    } else {
      c &= ((1 << first_bit) - 1);
    }
    bits->ptr[first_offset] = c;
    first_offset++;
    qws--;
  }

  if (last_bit != 63) {
    c = bits->ptr[last_offset];
    if (bit) {
      c |= ((2 << last_bit) - 1);
    } else {
      c &= ~((2 << last_bit) - 1);
    }
    bits->ptr[last_offset] = c;
    qws--;
  }

  if (qws>0) {
    memset(&bits->ptr[first_offset], bit ? 0xffffffffffffffff : 0x00, qws);
  }

  return;
}


void bs_fill(VALUE obj, struct BitSet *bits, int fill) {
  int i;
  VALUE vfirst, vend;
  int64_t first, end;
  switch(TYPE(obj)) {
  case T_FIXNUM:
    bs_set(bits, FIX2LONG(obj), fill);
    break;
  case T_ARRAY:
    for(i=0; i< (RARRAY_LEN(obj)); i++) {
      bs_fill( (RARRAY_PTR(obj)[i]), bits, fill);
    }
    break;
  default:
    if (rb_class_of(obj)==rb_cRange) {
      vfirst = rb_funcall(obj, id_first, 0);
      vend   = rb_funcall(obj, id_end, 0);
      Check_Type(vfirst, T_FIXNUM);
      Check_Type(vend, T_FIXNUM);
      first = FIX2LONG(vfirst);
      end   = FIX2LONG(vend);
      if (rb_funcall(obj, rb_intern("exclude_end?"), 0) == Qtrue) {
        end--;
      }
      if (first>end || first<0 || end<0) {
        rb_raise(rb_eRangeError, "not valid range");
      }
      bs_sets(bits, first, end, fill);
    } else {
      rb_raise(rb_eTypeError, "not valid index");
    }
  }
}

void bs_or(struct BitSet *bits, struct BitSet *x) {
  uint64_t *ptr, *xptr;
  int64_t i;

  if (bits->bit_len < x->bit_len) {
    bs_resize(bits, x->bit_len);
  }

  ptr   = bits->ptr;
  xptr  = x->ptr;
  for(i=0; i<bits->qws_len; i++) {
    *ptr++ |= *xptr++;
  }

}

void bs_not(struct BitSet *bits) {
  uint64_t *ptr;
  uint64_t qw;
  int64_t i;

  ptr   = bits->ptr;
  for(i=0; i < bits->qws_len; i++) {
    qw = *ptr;
    *ptr++ = ~qw;
  }
  if (bits->last_bit>0) {
    qw = *--ptr;
    *ptr = qw & ((1<<bits->last_bit)-1);
  }
}

/* Returned bitset is the size of the first operand. See README.md */

void bs_and(struct BitSet *rbits, struct BitSet *xbits, struct BitSet *ybits) {
  uint64_t *rptr, *xptr, *yptr;

  *rbits = *xbits;
  rbits->ptr = ALLOC_N(uint64_t, rbits->qws_len);

  rptr = rbits->ptr;
  xptr = xbits->ptr;
  yptr = ybits->ptr;

  for (int64_t ix = 0; ix < rbits->qws_len; ix++) {
    *rptr++ = *xptr++ & *yptr++;
  }
  if (rbits->last_bit > 0) {
      *(--rptr) &= (1<<rbits->last_bit)-1;
  }
}

void bs_xor(struct BitSet *bits, struct BitSet *x) {
  uint64_t *ptr, *xptr;
  int64_t i;

  if (bits->bit_len < x->bit_len) {
    bs_resize(bits, x->bit_len);
  }

  ptr   = bits->ptr;
  xptr  = x->ptr;
  for(i=0; i<bits->qws_len; i++) {
    *ptr++ ^= *xptr++;
  }

}

int64_t bs_max(struct BitSet *bits) {
  int64_t qws;
  int64_t pos;

  for (qws = bits->qws_len - 1; qws >= 0; qws -= 1) {
    asm (
          "bsr %1, %0 \n"
          "jnz bs_max_found \n"
          "mov $64, %0 \n"
          "bs_max_found: \n"
        : "=r"(pos) : "m"(bits->ptr[qws]) : "cc", "memory"
    );
    if (pos < 64) {
      return (qws * 64) + pos;
    }
  }
  return -1;
}

int64_t bs_min(struct BitSet *bits) {
  int64_t index;
  int pos;
  uint64_t *ptr, qw;

  ptr   = bits->ptr;
  for(index = 0; index < bits->bit_len; index += 64) {
    qw = *ptr++;
    asm (
          "bsf %1, %0 \n"
          "jnz bs_min_found \n"
          "mov $64, %0 \n"
          "bs_min_found: \n"
        : "=r"(pos) : "m"(qw) : "cc"
    );
    if (pos < 64) {
      return index + pos;
    }
  }
  return -1;
}

int64_t bs_index(struct BitSet *bits, int bit, int64_t off) {
  int64_t pos;

  asm (
      "mov %3, %%r9 \n"
      "shr $6, %%r9 \n" /* r9: loop counter stepping up in qw */
      "mov %2, %%r11 \n"
      "shr $6, %%r11 \n" /* r11: end marker in qw */
      "mov $18446744073709551615, %%r8 \n"
      "mov %3, %%rcx \n"
      "and $63, %%rcx \n" /* rcx: bit offset in first qw */
      "shl %%cl, %%r8 \n"
      "and (%1, %%r9, 8), %%r8 \n"
      "loop: \n"
      "  bsf %%r8, %%rax \n"
      "  jnz found \n"
      "  inc %%r9 \n"
      "  cmp %%r11, %%r9 \n"
      "  jl next \n"
      "  movq $-1, %0 \n"
      "  jmp exit \n"
      "next: \n"
      "  mov (%1, %%r9, 8), %%r8 \n"
      "  jmp loop \n"
      "found: \n"
      "  salq $6, %%r9 \n"
      "  add %%rax, %%r9 \n"
      "  movq %%r9, %0 \n"
      "exit: \n"
      : "=r"(pos) : "r"(bits->ptr), "r"(bits->bit_len), "r"(off), "m"(bit) : "%rax", "%rcx", "%r8", "%r9", "%r11", "cc", "memory"
  );

  return pos;
}
int to_bit(VALUE obj) {
  int bit;

  switch(TYPE(obj)) {
  case T_NIL:
  case T_FALSE:
    bit = 0;
    break;
  case T_FIXNUM:
    if (FIX2INT(obj)==0) {
      bit = 0;
    } else {
      bit = 1;
    }
    break;
  default:
    bit = 1;
  }

  return bit;
}

void bits_free(struct BitSet *bits) {
  ruby_xfree(bits->ptr);
}

static VALUE bits_s_new(int argc, VALUE *argv, VALUE self) {
  struct BitSet *bits;
  VALUE arg;
  VALUE obj;
  int64_t bytes;

  obj = Data_Make_Struct(self, struct BitSet, NULL, bits_free, bits);

  if (argc>0) {
    arg = argv[0];
  } else {
    arg = INT2FIX(1);
  }

  switch(TYPE(arg)) {
  case T_FIXNUM:
    bits->bit_len = FIX2LONG(arg);
    if (bits->bit_len < 1) {
      rb_raise(rb_eArgError, "array size");
    }
    bits->qws_len = (bits->bit_len+63)/64;
    bits->last_bit = bits->bit_len % 64;
    bits->ptr = ALLOC_N(uint64_t, bits->qws_len);
    memset(bits->ptr, 0, bits->qws_len*8);
    break;
  case T_STRING:
    bytes = RSTRING_LEN(arg);
    bits->bit_len = bytes*8;
    bits->qws_len = (bits->bit_len+63)/64;
    bits->last_bit = bits->bit_len % 64;
    bits->ptr = ALLOC_N(uint64_t, bits->qws_len);
    memcpy(bits->ptr, RSTRING_PTR(arg), bits->qws_len*8);
    break;
  default:
    rb_raise(rb_eArgError, "not valid value");
  }

  return obj;
}

static VALUE bits_s_from_bin(VALUE self, VALUE arg) {
  struct BitSet *bits;
  VALUE retobj;
  uint64_t qw, *bptr;
  unsigned char *ptr;
  int64_t len;
  int bitcnt;

  Check_Type(arg, T_STRING);

  len = RSTRING_LEN(arg);
  if (len<1) {
    rb_raise(rb_eArgError, "array size");
  }

  retobj = Data_Make_Struct(self, struct BitSet, NULL, bits_free, bits);
  bits->bit_len = len;
  bits->qws_len = (len+63)/64;
  bits->last_bit = len % 64;
  bits->ptr = ALLOC_N(uint64_t, bits->qws_len);
  memset(bits->ptr, 0, bits->qws_len*8);

  qw = 0;
  bitcnt = 0;
  ptr = (unsigned char *)RSTRING_PTR(arg);
  bptr = bits->ptr;
  while(len--) {
    switch(*ptr++) {
    case '0':
    case '-':
    case 'f':
    case 'F':
    case 'N':
      break;
    default:
      asm ( "bts %1, %0" : "=g"(qw) : "r"(bitcnt) : "cc","memory");
    }
    bitcnt++;
    if (bitcnt == 64) {
      *bptr++ = qw;
      qw = 0;
      bitcnt = 0;
    }
  }
  if (bitcnt) {
    *bptr++ = qw;
  }

  return retobj;
}


static VALUE bits_length(VALUE self) {
  struct BitSet *bits;

  Data_Get_Struct(self, struct BitSet, bits);

  return LONG2FIX(bits->bit_len);
}

static VALUE bits_bytesize(VALUE self) {
  struct BitSet *bits;

  Data_Get_Struct(self, struct BitSet, bits);

  return LONG2FIX(bs_bytesize(bits));
}

static VALUE bits_resize(VALUE self, VALUE obj) {
  struct BitSet *bits;
  int64_t len;

  Check_Type(obj, T_FIXNUM);
  len = FIX2LONG(obj);
  if (len<1) {
    rb_raise(rb_eArgError, "array size");
  }

  Data_Get_Struct(self, struct BitSet, bits);
  bs_resize(bits, len);

  return self;
}

static VALUE bits_to_s(VALUE self) {
  struct BitSet *bits;
  unsigned char *ptr;
  int64_t i;
  VALUE str;

  Data_Get_Struct(self, struct BitSet, bits);
  str = rb_str_new(0, bits->bit_len);

  ptr = (unsigned char *)RSTRING_PTR(str);
  for(i=0; i<bits->bit_len; i++) {
    *ptr++ = bs_get(bits, i) ? '1' : '0';
  }

  return str;
}

static VALUE bits_dup(VALUE self) {
  struct BitSet *orig;
  struct BitSet *dups;
  VALUE obj;
  int64_t qws;

  Data_Get_Struct(self, struct BitSet, orig);

  obj = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, dups);
  *dups = *orig;
  dups->ptr = ALLOC_N(uint64_t, dups->qws_len);
  memcpy(dups->ptr, orig->ptr, dups->qws_len*8);

  return obj;
}

static VALUE bits_clear(VALUE self) {
  struct BitSet *bits;

  Data_Get_Struct(self, struct BitSet, bits);
  memset(bits->ptr, 0, bits->qws_len*8);

  return self;
}

static VALUE bits_get(VALUE self, VALUE vidx) {
  struct BitSet *bits;
  int64_t index;
  int bit;

  Data_Get_Struct(self, struct BitSet, bits);

  Check_Type(vidx, T_FIXNUM);

  index = FIX2LONG(vidx);
  if (index<0) {
    rb_raise(rb_eRangeError, "index range");
  }

  bit = bs_get(bits, index);

  return INT2FIX(bit);
}


static VALUE bits_set(VALUE self, VALUE vidx, VALUE vobj) {
  struct BitSet *bits;
  int64_t index;
  int bit;

  Data_Get_Struct(self, struct BitSet, bits);

  Check_Type(vidx, T_FIXNUM);

  index = FIX2LONG(vidx);
  if (index<0) {
    rb_raise(rb_eRangeError, "index range");
  }

  bit = to_bit(vobj);

  bs_set(bits, index, bit);

  return self;
}

/* This method returns the index of the first bit set to the given value */

static VALUE bits_index(VALUE self, VALUE vobj, VALUE voff) {
  struct BitSet *bits;
  int64_t       pos, off;
  int           bit;

  Data_Get_Struct(self, struct BitSet, bits);
  bit = to_bit(vobj);
  Check_Type(voff, T_FIXNUM);
  off = FIX2LONG(voff);

  pos = bs_index(bits, bit, off);

  return (pos == -1) ? Qnil : LONG2FIX(pos);
}
/* This method extracts a subset of the bitset */

static VALUE bits_slice(VALUE self, VALUE vidx) {
  struct BitSet *bits;
  struct BitSet *slis;
  VALUE obj;
  VALUE vfirst, vend;
  int64_t first, end;
  int64_t ix, ox;
  int64_t qws;

  Data_Get_Struct(self, struct BitSet, bits);

  /* Validate the range and extract start and end */

  if (rb_class_of(vidx)==rb_cRange) {
    vfirst = rb_funcall(vidx, id_first, 0);
    vend   = rb_funcall(vidx, id_end, 0);
    Check_Type(vfirst, T_FIXNUM);
    Check_Type(vend, T_FIXNUM);
    first = FIX2LONG(vfirst);
    end   = FIX2LONG(vend);
    if (rb_funcall(vidx, rb_intern("exclude_end?"), 0) == Qtrue) {
      end--;
    }
    if (first>end || first<0 || end<0) {
      rb_raise(rb_eRangeError, "not valid range");
    }
  } else {
    rb_raise(rb_eTypeError, "not valid index");
  }

  obj = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, slis);
  slis->bit_len = end - first + 1;
  slis->qws_len = (slis->bit_len+63)/64;
  slis->last_bit = slis->bit_len % 64;
  slis->ptr = ALLOC_N(uint64_t, slis->qws_len);

  /* Now copy the bits TBD use asm */

  for (ix = first, ox = 0; ix <= end; ix++,ox++) {
    bs_set(slis, ox, bs_get(bits, ix));
  }
  return obj;
}

static VALUE bits_on(int argc, VALUE *argv, VALUE self) {
  struct BitSet *bits;
  int i;
  Data_Get_Struct(self, struct BitSet, bits);

  for(i=0; i<argc; i++) {
    bs_fill(argv[i], bits, 1);
  }

  return self;
}

static VALUE bits_off(int argc, VALUE *argv, VALUE self) {
  struct BitSet *bits;
  int i;

  Data_Get_Struct(self, struct BitSet, bits);

  for(i=0; i<argc; i++) {
    bs_fill(argv[i], bits, 0);
  }

  return self;
}

static VALUE bits_or(VALUE self, VALUE other) {
  VALUE obj;
  struct BitSet *bits, *rbits, *xbits;

  Data_Get_Struct(self, struct BitSet, bits);

  obj = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, rbits);
  *rbits = *bits;
  rbits->ptr = ALLOC_N(uint64_t, rbits->qws_len);
  memcpy(rbits->ptr, bits->ptr, rbits->qws_len*8);

  Data_Get_Struct(other, struct BitSet, xbits);
  bs_or(rbits, xbits);

  return obj;
}

static VALUE bits_not(VALUE self) {
  VALUE obj;
  struct BitSet *bits, *rbits;

  Data_Get_Struct(self,  struct BitSet, bits);

  obj = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, rbits);
  *rbits = *bits;
  rbits->ptr = ALLOC_N(uint64_t, rbits->qws_len);
  memcpy(rbits->ptr, bits->ptr, rbits->qws_len*8);

  bs_not(rbits);

  return obj;
}

static VALUE bits_and(VALUE self, VALUE other) {
  VALUE r;
  struct BitSet *rbits, *xbits, *ybits;

  Data_Get_Struct(self,  struct BitSet, xbits);
  Data_Get_Struct(other, struct BitSet, ybits);

  r = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, rbits);

  bs_and(rbits, xbits, ybits);

  return r;
}

static VALUE bits_xor(VALUE self, VALUE other) {
  VALUE obj;
  struct BitSet *bits, *rbits, *xbits;

  Data_Get_Struct(self,  struct BitSet, bits);

  obj = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, rbits);
  *rbits = *bits;
  rbits->ptr = ALLOC_N(uint64_t, rbits->qws_len);
  memcpy(rbits->ptr, bits->ptr, rbits->qws_len*8);

  Data_Get_Struct(other, struct BitSet, xbits);
  bs_xor(rbits, xbits);

  return obj;
}

/* r = a and ~b */
static VALUE bits_minus(VALUE self, VALUE other) {
  VALUE r;
  struct BitSet i;
  struct BitSet *rbits, *xbits, *ybits, *ibits;

  Data_Get_Struct(self,  struct BitSet, xbits);
  Data_Get_Struct(other, struct BitSet, ybits);
  r = Data_Make_Struct(CLASS_OF(self), struct BitSet, NULL, bits_free, rbits);

  ibits = &i;
  *ibits = *ybits;
  ibits->ptr = ALLOC_N(uint64_t, ibits->qws_len);
  memcpy(ibits->ptr, ybits->ptr, ibits->qws_len*8);

  if (xbits->bit_len > ybits->bit_len) bs_resize(ibits, xbits->bit_len);
  bs_not(ibits);
  bs_and(rbits, xbits, ibits);

  return r;
}

static VALUE bits_cmp(VALUE self, VALUE other) {
  struct BitSet *sbits, *obits;
  int64_t smax,omax,qws;
  uint64_t *sptr,*optr, sqw, oqw;

  Data_Get_Struct(self,  struct BitSet, sbits);
  Data_Get_Struct(other, struct BitSet, obits);

  smax = bs_max(sbits);
  omax = bs_max(obits);
  if (smax < omax) {
    return INT2FIX(-1);
  } else if (smax > omax) {
    return INT2FIX(1);
  }

  if (smax < 0) {
    return INT2FIX(0);
  }

  qws = (smax+63)/64;
  sptr = sbits->ptr + qws - 1;
  optr = obits->ptr + qws - 1;
  while(qws--) {
    sqw = *sptr--;
    oqw = *optr--;
    if (sqw < oqw) {
      return INT2FIX(-1);
    } else if (sqw > oqw) {
      return INT2FIX(1);
    }
  }

  return INT2FIX(0);
}


static VALUE bits_zero(VALUE self) {
  struct BitSet *bits;
  uint64_t *ptr;
  int64_t i;

  Data_Get_Struct(self,  struct BitSet, bits);

  ptr = bits->ptr;
  for(i=0 ; i<bits->qws_len; i++) {
    if (*ptr++ != 0) {
      return Qfalse;
    }
  }

  return Qtrue;
}

static VALUE bits_nonzero(VALUE self) {
  struct BitSet *bits;
  uint64_t *ptr;
  int64_t i;

  Data_Get_Struct(self,  struct BitSet, bits);

  ptr = bits->ptr;
  for(i=0 ; i<bits->qws_len; i++) {
    if (*ptr++ != 0) {
      return Qtrue;
    }
  }

  return Qfalse;
}

static VALUE bits_max(VALUE self) {
  struct BitSet *bits;
  int64_t index;

  Data_Get_Struct(self, struct BitSet, bits);

  index = bs_max(bits);

  if (index >= 0) {
    return LONG2FIX(index);
  } else {
    return Qnil;
  }
}

static VALUE bits_min(VALUE self) {
  struct BitSet *bits;
  int64_t index;

  Data_Get_Struct(self, struct BitSet, bits);

  index = bs_min(bits);

  if (index >= 0) {
    return LONG2FIX(index);
  } else {
    return Qnil;
  }
}

static VALUE bits_norm(VALUE self) {
  VALUE obj;
  struct BitSet *bits;
  int64_t index;

  obj = bits_dup(self);
  Data_Get_Struct(obj, struct BitSet, bits);

  index = bs_max(bits);
  if (index < 0) {
    index = 1;
  } else {
    index = index + 1;
  }
  bs_resize(bits, index);

  return obj;
}

static VALUE bits_normx(VALUE self) {
  struct BitSet *bits;
  int64_t index;

  Data_Get_Struct(self, struct BitSet, bits);

  index = bs_max(bits);

  if (index < 0) {
    index = 1;
  } else {
    index = index + 1;
  }
  bs_resize(bits, index);

  return self;
}

static VALUE bits_to_ary(VALUE self) {
  VALUE ary;
  struct BitSet *bits;
  uint64_t *ptr, c;
  int64_t from, to, rest, index, len, cnt;

  Data_Get_Struct(self,  struct BitSet, bits);

  ary = rb_ary_new();

  for (index = 0; index != -1;) {
      index = bs_index(bits, 1, index);
      if (index < 0) break;
      from = index;
      to = from;
      do {
          index = bs_index(bits, 1, index+1);
          if (index == -1) break;
          if (index == to + 1) to = index;
      } while (index == to);

      if (from==to) {
        rb_ary_push(ary, LONG2FIX(from));
      } else {
        rb_ary_push(ary, rb_funcall(rb_cRange, id_new, 2, LONG2FIX(from), LONG2FIX(to)));
      }
  }
  return ary;
}

static VALUE bits_to_bytes(VALUE self) {
  struct BitSet *bits;

  Data_Get_Struct(self,  struct BitSet, bits);

 return rb_str_new((char *)bits->ptr, bs_bytesize(bits));
}

static VALUE bits_each(VALUE self) {
  struct BitSet *bits;
  int64_t idx;

  Data_Get_Struct(self,  struct BitSet, bits);

  for(idx=0; idx < bits->bit_len; idx++) {
    if (bs_get(bits, idx)) {
      rb_yield(LONG2FIX(idx));
    }
  }

  return self;
}

static VALUE bits_count(VALUE self) {
  struct BitSet *bits;
  uint64_t *ptr, qw;
  int64_t count;
  int64_t i;

  Data_Get_Struct(self,  struct BitSet, bits);

  count = 0;
  ptr = bits->ptr;
  for(i = 0 ; i < bits->qws_len; i++) {
    qw = *ptr++;
    asm ( "mov %1, %%r8 \n"
          "bs_count: \n"
          "bsf %%r8, %%r11 \n"
          "jz bs_count_exit \n"
          "inc %0 \n"
          "btr %%r11, %%r8 \n"
          "jmp bs_count \n"
          "bs_count_exit: \n"
        : "=&r"(count) : "r"(qw) : "%r8", "cc", "memory"
    );
  }

  return LONG2FIX(count);
}

void Init_bitset() {
  cBitSet =  rb_define_class("BitSet", rb_cObject);
  rb_include_module(cBitSet, rb_mComparable);

  rb_define_singleton_method(cBitSet, "new", bits_s_new, -1 );
  rb_define_singleton_method(cBitSet, "from_bin", bits_s_from_bin, 1 );

  rb_define_method(cBitSet, "size",       bits_length,    0);
  rb_define_method(cBitSet, "size=",      bits_resize,    1);
  rb_define_method(cBitSet, "length",     bits_length,    0);
  rb_define_method(cBitSet, "bytesize",   bits_bytesize,  0);
  rb_define_method(cBitSet, "to_s",       bits_to_s,      0);
  rb_define_method(cBitSet, "dup",        bits_dup,       0);
  rb_define_method(cBitSet, "clone",      bits_dup,       0);
  rb_define_method(cBitSet, "slice",      bits_slice,     1);
  rb_define_method(cBitSet, "index",      bits_index,     2);
  rb_define_method(cBitSet, "clear",      bits_clear,     0);
  rb_define_method(cBitSet, "get",        bits_get,       1);
  rb_define_method(cBitSet, "set",        bits_set,       2);
  rb_define_method(cBitSet, "on",         bits_on,       -1);
  rb_define_method(cBitSet, "off",        bits_off,      -1);

  rb_define_method(cBitSet, "|",          bits_or,        1);
  rb_define_method(cBitSet, "~",          bits_not,       0);
  rb_define_method(cBitSet, "&",          bits_and,       1);
  rb_define_method(cBitSet, "^",          bits_xor,       1);
  rb_define_method(cBitSet, "+",          bits_or,        1);
  rb_define_method(cBitSet, "-",          bits_minus,     1);
  rb_define_method(cBitSet, "*",          bits_and,       1);
  rb_define_method(cBitSet, "<=>",        bits_cmp,       1);

  rb_define_method(cBitSet, "zero?",      bits_zero,      0);
  rb_define_method(cBitSet, "nonzero?",   bits_nonzero,   0);
  rb_define_method(cBitSet, "max",        bits_max,       0);
  rb_define_method(cBitSet, "min",        bits_min,       0);
  rb_define_method(cBitSet, "normalize",  bits_norm,      0);
  rb_define_method(cBitSet, "normalize!", bits_normx,     0);

  rb_define_method(cBitSet, "to_ary",     bits_to_ary,    0);
  rb_define_method(cBitSet, "to_bytes",   bits_to_bytes,  0);
  rb_define_method(cBitSet, "count",      bits_count,     0);

  rb_define_method(cBitSet, "each",       bits_each,      0);
  /* rb_include_module(cBitSet, rb_mEnumerable); */

  id_new   = rb_intern("new");
  id_first = rb_intern("first");
  id_end   = rb_intern("end");
}
