require "mkmf"

with_cflags(ENV['DEBUG'] ? ' -Wextra -O0 -ggdb3':'' ) do
  create_makefile("bitset")
end
