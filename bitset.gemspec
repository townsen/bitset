$:.push File.expand_path("../lib", __FILE__)

require 'bitset/version'

BitSet::GemSpec = Gem::Specification.new do |s|
  s.name        = %q{bitset}
  s.version     = BitSet::VERSION
  s.required_ruby_version = ">=2.0.0"
  s.date        = Time.now.strftime("%Y-%m-%d")
  s.authors     = ['Nick Townsend']
  s.email       = ['nick.townsend@mac.com']
  s.summary     = %q{Ruby BitSet Implementation}
  s.homepage    = %q{https://github.com/townsen/bitset/}
  s.has_rdoc    = false
  s.files       = Dir['lib/**/*.rb'] + Dir['test/**/*.rb'] + Dir['ext/**/*']
  s.files       += %w{ Rakefile README.md .gemtest }
  s.description = "BitSet for Ruby implemented in C"
  s.extensions  = ["ext/bitset/extconf.rb"]
  s.license     = 'MIT'
  s.add_development_dependency 'minitest', '~> 4.3', '>= 4.3.2'
end
