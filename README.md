# bitset

A BitSet implementation for Ruby using C with inline assembler for speed

# Notes on BitSet Lengths

Many bitset operations are only defined if the sets are the same size. By
considering a bitset to possess any number of high-order zeros most operations
can be made to work with differently sized operands. When creating this library
a couple of different approaches to this were considered:

* Return a result the size of the first operand,
* Return the smallest result set (not necessarily normalized),
* Return a normalized result (the length being the position of the high order bit).

Like the original implementor I chose to use the middle strategy.

# Operators and Methods
Statements that use binary operators of the form `r = a op b` are computed by
the Ruby call `a.op(b)`

Most operators are obvious, here's some that aren't

## minus
This is defined elementwise as: r = a and not b

## to_bytes
This returns a string consisting of 8-bit bytes that represents the string. Note that this
is not the binary representation as a string of 0's and 1's. For that use the method `from_bin`
and `to_s`

## bytesize
This returns the length in bytes required to store this bitset
